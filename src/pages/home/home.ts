import { Component } from '@angular/core';
import { NavController , AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	/* Arrays */
	public extrato: Array<any> = [];
	public table: Array<any> = [];
 
	constructor(
		public navCtrl: NavController, 
		private alertCtrl: AlertController
	){}
	/* Funções */
	adicionaMissa(){
		this.table.carrega();	
		this.notify('Adicionado com Sucesso!','Missa computadada <br> dia: xx/xx/xxx <br> hora: xx:xx');
	}
	
	/* Objetos */
	table = {
		carrega: function(){
			console.log('pegou');
			if (typeof(Storage) !== "undefined") {
				if(localStorage.getItem("DB") !== null){
					console.log('Banco de dados encontrado','Tudo OK');
					console.log(JSON.parse(localStorage.getItem('DB')));
				}else{
					console.log('Nenhum Banco de dados Encontrado','Erro');
					localStorage.setItem('DB','{}')
					console.log(JSON.parse(localStorage.getItem('DB')));
				}
			} else {
				console.log('Problema Grave!!','Seu celular não pode computar suas missas. Nos desculpe!');
			}
			
		},
		exibe:function(){
			
		},
		edita:function(){
			
		},
		remove:function(){
			
		},
		adiciona:function(){
			const entry = new Entry();
			entry.id = this.extrato.length + 1;
			entry.dia = 0;
			entry.mes = 0;
			entry.ano = 0;
			entry.hora = 0;
			entry.minuto = 0;			
			this.extrato.push(entry);
		},
		salva:function(){
			
		}
	};
	
	public notify(titulo: string,message: string,success: boolean = true) :void {
		this.alertCtrl.create({
		title: titulo,
		subTitle: message,
		buttons: ['OK']
		}).present();
	}

	

}
